# TP1: OCL - FAR ★ STAR

L'objectif de ce TP est d'implémenter et de vérifier un ensemble de contraintes OCL
sur une implémentation d'un système de gestion de fret spatial.

## Diagramme de classes

Voici le diagramme de classes sur lequel vous allez devoir ajouter les contraintes OCL.

<a href="uml.png">
  <img src="uml.png" alt="Diagramme de classes" style="width: 100%" />
</a>

## Contraintes à implémenter

### CommandCenter

* `CommandCenter1`: Il ne peut exister qu'une seule instance du `CommandCenter`.

* `CommandCenter2`: Tous les vaisseaux enregistrés dans le command center
  (relation `CommandCenterShips`) sont immatriculés par la féeration (`id`
  entre `#F0000000` et `#FFFFFFFF`).

* `CommandCenter3`: Tous les vaisseaux immatriculés par la fédération (`id`
	entre `#F0000000` et `#FFFFFFFF` sont enregistrés dans le command center
	(relation `CommandCenterShips`).

* La méthode `CommandCenter::register(ship: Ship)` permet d'enregistrer un vaisseau
  dans le command center.

  * `CommandCenter4`: Pour appeler la méthode `register(ship)`, le vaisseau
	(`ship`) ne doit pas déjà être enregistré.

  * `CommandCenter5`: Avec la méthode `register(ship)`, on ne peut enregistrer
	que des vaisseaux immatriculés par la fédération.

  * `CommandCenter6`: Une fois la méthode `register(ship)` appelée, le vaisseau
	se trouve dans la liste des vaisseaux enregistrés par la fédération.

### Item

* `Item1`: Un object (`Item`) à une masse (`mass`) strictement positive.

### Ship

* `Ship1`: Un vaisseau (`Ship`) possède toujours une immatriculation (`id`).

* `Ship2`: L'immatriculation (`id`) d'un vaisseau est une chaîne de 8 caractères hexadécimaux
  en majuscules.

* `Ship3`: Deux vaisseaux ne peuvent avoir la même immatribulation (`id`).

* `Ship4`: Un vaisseau à une coque (`hull`) entre 0 et 100 inclus.

* `Ship5`: Un vaisseau possède un champs de force (`field`) entre 0 et 10 inclus.

* La méthode `isDestroyed(): Bool` indique si le vaisseau est détruit.

  * `Ship6`: La méthode `isDestroyed()` retourne `true` si `hull` vaut 0.

* La méthode `damage(hull): Bool` permet d'endommager la coque d'un vaisseau.

	* `Ship7`: La méthode attend un nombre de point de coque (`hull`) à retirer
	  strictement supérieur à 0.

	* `Ship8`: Après l'appel de la méthode, le champs de force du vaisseau (`Ship::Field`)
	  a baissé de `hull`.
	  Rappel: le champs de force d'un vaisseau est toujours compris entre 0 et 10.

	* `Ship9`: Après l'appel de la méthode, les points de coque du vaisseau (`Ship::hull`) ont
	  baissé de `hull` moins la puissance du champs de force (`Ship::field`).
	  Rappel: la coque d'un vaisseau est toujours comprise entre 0 et 100.

	* `Ship10`: La méthode retourne `true` si le vaisseau a été endommagé.

### CombatShip

* La méthode `CombatShip::equip(weapon: Weapon)` permet à un vaisseau d'équiper une arme.

	* `CombatShip1`: Avec la méthode `CombatShip::equip(weapon: Weapon)`, les vaisseaux de combat
	  légers (`LightCombat`) peuvent équiper jusqu'à 2 armes.
	  Les vaisseaux de combat lourds (`HeavyCombat`) peuvent équiper jusqu'à 10 armes.

	* `CombatShip2`: Un vaisseau de combat ne peut pas équiper une arme déjà équipée
	  par un autre vaisseau ou par lui même.

	* `CombatShip3`: Les vaisseaux de combat légers (`LightCombat`) ne peuvent pas
	  équiper de `Blasters`.

	* `CombatShip4`: Une fois la méthode `equip(weapon: Weapon)` appelée,
	  le vaisseau contient l'arme (`weapon`) passée en paramètre.

	* `CombatShip5`: La masse (`mass`) du vaisseau est modifié en conséquence.

* `CombatShip6`: Le poids (`mass`) d'un vaisseau de combat est toujours de son
  poids de base plus le poids de toutes ses armes.
  Le poids de base est de 10 pour les `LightCombat` et 100 pour les `HeavyCombat`.

### Weapon

* `Weapon1`: Les `Blasters` on un niveau de `plasma` entre 0 et 10 inclus.

* `Weapon2`: La méthode `Weapon::canFire(): Boolean` retourne toujours vrai pour les Phasers.
  Si l'arme est une `Blaster`, elle retourne vrai seulement si le niveau de
  `plasma` du blaster est suppérieur à 0.

* `Weapon3`: Pour pouvoir tirer avec la méthode `Weapon::fire(): Integer`,
   les blasters doivent avoir au moins 1 point de plasma.
   Les phasers peuvent toujours tirer.

* `Weapon4`: La méthode `Weapon::fire(): Integer` retourne le nombre de points
  de dommages générés par l'arme.
  Quand il tire, le blaster génère autant de points de dommages qu'il a de plasma.
  Les phasers inflige génèrent toujours 2 points de dommages.

* `Weapon5`: Quand il tire avec la méthode `Weapon::fire(): Integer`, le blaster
  perd tous ses points de plasma.

### TransportShip

* `TransportShip1`: La charge maximale (`maxLoad`) d'un vaisseau de transport
  est strictement suppérieur à 0.

*  `TransportShip2`: La charge actuelle (`currentLoad`) d'un vaisseau de transport
  est toujours supérieure ou égale à 0 et inférieure ou égale à `maxLoad`.

* `TransportShip3`: La charge actuelle (`currentLoad`) d'un vaisseau de transport
  est toujours égale à la somme des masses (`mass`) des `items` qu'elle contient.

* La méthode `TransportShip::load(item: Item)` permet de charger un objet dans
  vaisseau de transport.

	* `TransportShip4`: L'objet à charger (`item`) ne doit pas faire dépasser la charge
	  maximale (`maxLoad`)

	* `TransportShip5`: L'objet à charger (`item`) ne doit pas déjà se trouver dans un
	  autre vaisseau ou dans le vaisseau courrant.

	* `TransportShip6`: Après l'appel, la charge actuelle (`currentLoad`) a été modifiée.

	* `TransportShip7`: Après l'appel, le poids (`mass`) du vaisseau a été modifié.

	* `TransportShip8`: Après l'appel, l'objet (`item`) se trouve dans les objets
	  du vaisseau (`items`).

Pour chaque contrainte, vous utiliserez le contexte qui vous semblera le plus approprié.
Les contextes utisés ci-dessus ne sont là que dans un but de catégorisation et de clareté.

### Réalisation

La réalisation de ce travail se fera par groupes d'au plus **deux** personnes.
Chaque membre de l'équipe devra maîtriser **tous** les aspects du travail.

Le TP est à réaliser avec **USE-OCL** et sera corrigé avec ce même logiciel.
Vous devez utiliser les fichiers de modèle et d'instance disponibles sur
[Github](https://github.com/Morriar/INF3143_173_TP1).

### Remise

Vous devez remettre **un seul document OCL** contenant toutes les contraintes
présentées ci-dessus en respectant les règles suivantes:
* le fichier se nomme `constraints.use`
* l'entête du fichier contient en commentaire les noms, prénoms et codes permanents
  de chacun des membres du groupe
* le fichier commence par la contrainte `constraints`

Pour chaque contrainte:
* vous utiliserez le code de la règle (ex: `Player1`, `Round2`) comme identifiant de la contrainte
* vous utiliserez des commentaires pour expliquer les parties complexes de vos contraintes

Voici un exemple de remise correctement présentée:

	-- OCL TP1
	-- TERA05028700 - Alexandre Terrasa

	constraints

	context Etudiant
		-- Le code permanent de l'étudiant doit faire exactement 6 caractères.
		inv Contrainte1: code.size() = 6

Le non-respect de ces règles entrainera automatiquement une perte de points.

Le document est à rendre:
1. en version papier (pas d'enveloppe, agrafes uniquement)
2. **ET** en version électronique sur Oto: [Remise en ligne](http://oto.labunix.uqam.ca/application-web/connexion) ([Manuel d'utilisation](http://oto.labunix.uqam.ca/documents/manuel-oto-etudiant.pdf))
    * Nom du professeur à utiliser : `terrasa_a`
    * Nom de la boite oto : `INF3143_TP1`
	* Nom du fichier à remettre : `constraints.use`
    * Vous pouvez faire autant de remises que vous voulez (seule la dernière est conservée).

Ce travail est à rendre **au plus tard le 25 octobre 2017** au début du cours.

Vous pouvez aussi déposer votre travail dans la chute à travaux du pavillon Président-Kennedy (4ème étage, à côté des ascenceurs).
Le tampon du secrétariat fera foi quant à la date et heure de remise.

### Évaluation et barème indicatif

* Il y a 35 contraintes à implémenter chacune valant entre 1, 2 et 3 points.
* La note finale est sur 100.
* La qualité du code OCL, la propreté du rendu ainsi que la qualité du français sont évalués.

#### Pénalités

* Fichier de contraintes rendues sans code permanent: -100%
* Fichier de contraintes ne compilant pas avec USE 4.2: -100%
* Fichier de contraintes ne commençant pas par `constraints`: -50%
* Fichier de contraintes ne contenant pas les identifiants des contraintes: -50%
* Pas de version électronique: -100%
* Pas de version papier: -50%
* Pénalité par heure de retard: -10%
